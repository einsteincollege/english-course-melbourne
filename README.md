English language training program at Einstein College is designed to develop students' ability to communicate confidently in English for a wide range of situations including travel, work and social interactions.

We offer English certificate courses for Basic, Intermediate and Advanced Level students. We target on reading, writing, speaking and listening.
Students from any part of the world can enrol to our English course from 1-52 weeks depending upon their VISA conditions.
We have new batches starting every Monday. 

Visiting and Working holiday visa holders can enrol in English training from 1 week to 4 months depending on their VISA conditions.  
The above VISA holders can also convert their visa to a student visa subject to the visa requirements and eligibility.

Note: Please contact the college, if you need more information.
Entry Requirements

No IELTS Requirements
At least 18 years old
Valid VISA to study in Australia +
